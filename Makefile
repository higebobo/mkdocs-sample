MAKEFLAGS += --warn-undefined-variables
SHELL := /bin/bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := build-view

# all targets are phony
.PHONY: $(shell egrep -o ^[a-zA-Z_-]+: $(MAKEFILE_LIST) | sed 's/://')

DOC_DIR = doc

# .env
ifneq ("$(wildcard ./.env)","")
  include ./.env
endif

pip: ## Install package by pip
	@pip install -r requirements.txt

create: ## Create mkdocs project (only first time)
	@mkdocs new ${DOC_DIR}

serve: ## Serve mkdocs documents
	@mkdocs serve

build-view: build view ## Build and view pdf

build: ## Build mkdocs documents
	@mkdocs build

view: ## Build mkdocs documents
	@evince site/pdf/document.pdf

help: ## Print this help
	@echo 'Usage: make [target]'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
